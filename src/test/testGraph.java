
package test;
import static org.junit.Assert.*;
import org.junit.*;

import model.data_structures.Graph;
public class testGraph {

	private Graph graph = new Graph<>();
	
	private void setUp()
	{
		graph.addVertex(1, "Vert 1", 0);
		graph.addVertex(2, "Vert 2", 0);
		graph.addVertex(3, "Vert 3", 0);
		graph.addVertex(4, "Vert 4", 0);
		graph.addVertex(5, "Vert 5", 0);
	}

	@Test
	public void testeo()
	{
		setUp();
		assertEquals("El tama�o de vertex est� mal",5, graph.vSize());
		
		graph.addEdge(1, 2, "1-2");
		graph.addEdge(3, 4, "3-4");
		graph.addEdge(5, 1, "5-1");
		
		assertEquals("El tama�o de edges est� mal",3,graph.eSize());
		
		assertEquals("La info del edge est� mal", "1-2", graph.getInfoEdge(1, 2));
		
		graph.setInfoEdge(1, 2, "2-1");
		
		assertEquals("El set del edge est� mal", "2-1", graph.getInfoEdge(1, 2));
		
		assertEquals("La info del vertex est� mal", "Vert 1", graph.getInfoVertex(1));
		
		graph.setInfoVertex(1, "Vertice 1");
		
		assertEquals("El set del vertex est� mal", "Vertice 1", graph.getInfoVertex(1));
		
		assertNotNull(graph.adj(1));
		
		assertEquals("Tiene un degree erroneo", 2, graph.degree(1));
		
		
	}
}
