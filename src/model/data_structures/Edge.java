package model.data_structures;

public class Edge <K extends Comparable<K>, V, A>{

	/**
	 * Vertice donde inicia
	 */
	private Vertex startVertex;
	
	/**
	 * Vertice donde termina
	 */
	private Vertex endVertex;
	
	/**
	 * Informacion del edge
	 */
	private Object info;
	
	private K startVertexId;
	
	private K endVertexId;
	
	/**
	 * Crea un nuevo edge
	 * @param v1 Vertice inicial
	 * @param v2 Vertice final
	 * @param a Informacion del vertice
	 */
	public Edge(Vertex v1, Vertex v2, Object a) {
		startVertex=v1;
		endVertex=v2;
		info=a;	
	}
	
	public Edge(K iv1, K iv2, Object a) {
		startVertexId=iv1;
		endVertexId=iv2;
		info=a;
	}
	
	/**
	 * Crea un nuevo edge sin informacion
	 * @param v1 Vertice inicial
	 * @param v2 Vertice final
	 */
	public Edge(Vertex v1, Vertex v2) {
		startVertex=v1;
		endVertex=v2;
		info=null;	
	}
	
	/**
	 * Retorna el vertice inicial
	 */
	public Vertex getStartVertex() {
		return startVertex;
	}

	/**
	 * Retorna el vertice final
	 */
	public Vertex getEndVertex() {
		return endVertex;
	}
	
	/**
	 * Retorna la informacion del edge
	 */
	public Object getInfo() {
		return info;
	}
	
	/**
	 * Permite asignar una informacion al edge con una recibida por parametro
	 * @param pInfo Informacion a asignar
	 */
	public void setInfo(Object pInfo) {
		info = pInfo;
	}
	
	public Edge actuallyBecomeEdge(Graph graph) {
		startVertex=graph.getVertex(startVertexId);
		endVertex=graph.getVertex(endVertexId);
		return this;
	}
}
