package model.data_structures;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PrintGraph {
	
	private Graph graph;
	
	public PrintGraph(Graph pGraph)
	{
		graph = pGraph;
	}
	
	public void imprimir()
	{
		File archivo = new File("./data/mapa.html");
		PrintWriter writer;
		try {
			writer = new PrintWriter(archivo);
			
			writer.println(" <!DOCTYPE html>\n");
			writer.println("<html>\n");
			writer.println("<head>\n");
			writer.println("<meta charset=utf-8 />\n");
			writer.println("<title>A simple map</title>\n");
			writer.println("<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />\n");
			writer.println("<script src='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.js'></script>\n");
			writer.println("<link href='https://api.mapbox.com/mapbox.js/v3.1.1/mapbox.css' rel='stylesheet' />\n");
			writer.println("<style>\n");
			writer.println("body { margin:0; padding:0; }\n");
			writer.println("#map { position:absolute; top:0; bottom:0; width:100%; }\n");
			writer.println("</style>\n");
			writer.println("</head>\n");
			writer.println("<body>\n");
			writer.println("<div id='map'></div>\n");
			writer.println("<script>\n");
			writer.println("L.mapbox.accessToken = 'pk.eyJ1IjoiYmFiYXRpdmEiLCJhIjoiY2pvZHQzOXBqMTFwaTNwcnRkc2M5NWpwNCJ9.9q0ifj4ObIgLVSg3qfviJw';\n");
			writer.println("var map = L.mapbox.map('map', 'mapbox.streets').setView([41.5100, -87.39], 9);\n");
			for(int i = 0; i<graph.vSize();i++)
			{
				Vertex v = graph.getVertex(i);
				if(v!=null)
				{
					String info = (String) v.getValue();
					String[] splits = info.split(":");
					String circulo = "var circ = L.circle([" + splits[1] + ", " + splits[0] + "], 50).addTo(map);"	;
					writer.println(circulo);
					ListIterator<Edge> ite = v.getEdgesList().iterator();
					if(ite!=null)
					{
						while(ite.hasNext())
						{
							Edge actual=ite.next();
							Vertex ini = actual.getStartVertex();
							Vertex fini = actual.getEndVertex();
							String infoIni = (String) ini.getValue();
							String[] splitsIni = infoIni.split(":");
							String infoFini = (String) fini.getValue();
							String[] splitsFini = infoFini.split(":");
							writer.println("var localizaciones = [[" + splitsIni[1] + "," + splitsIni[0] + "],[" + splitsFini[1] + "," + splitsFini[0] + "] ];");
							writer.println("var pol = L.polyline( localizaciones , { color:'#ff0000' } ).addTo(map);");

						}
					}
					
				}
			}
			
			
			writer.println("</script>\n");
			writer.println("</body>\n");
			writer.println("</html>\n");
			writer.close();
			System.out.println("final");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
