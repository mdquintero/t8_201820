package model.data_structures;
import java.io.InputStream;
import java.util.NoSuchElementException;

import org.w3c.dom.ls.LSInput;

import model.logic.Manager;

/**
 * Clase que representa un grafo no dirigido
 * @authors Mateo Quintero, Daniel Babativa
 *
 * @param <K> Llave de un vertice
 * @param <V> Valor de un vertice
 * @param <A> Informacion de un arco
 */

public class Graph <K extends Comparable<K>, V, A> {

	private static final String NEWLINE = System.getProperty("line.separator");

	private int vSize;
	private int eSize;
	private LinearProbingTH<K, V> vertexTH;
	private LinearProbingTH<String,Edge> edgesTH;

	/**
	 * Crea un grafo NO dirigido sin vértices y sin arcos
	 */
	public Graph() {
		this.vSize = 0;
		this.eSize = 0;
		vertexTH = new LinearProbingTH<K,V>();
		edgesTH = new LinearProbingTH<String,Edge>();
	}

	/**
	 * Returns el numero de vertices en el grafo.
	 *
	 * @return el numero de vertices en el grafo
	 */
	public int vSize() {
		return vSize;
	}

	/**
	 * Retorna el numero de edges en el grafo.
	 *
	 * @return el numero de edges en el grafo
	 */
	public int eSize() {
		return eSize;
	}

	/**
	 * Agrega un vertice al grafo
	 * @param idVertex Id del vertice a agregar
	 * @param infoVertex Informacion del vertice a agregar
	 */
	public void addVertex(K idVertex, V infoVertex, int n) {
		Vertex vert = new Vertex(idVertex,infoVertex);
		if(n==0)
			vert.setTipo(Vertex.Tipo.INTERSECCION);
		else
			vert.setTipo(Vertex.Tipo.ESTACION);
		vertexTH.put(idVertex, (V) vert);
		vSize++;
	}

	public void addVertex(Vertex v) {
		vertexTH.put((K)v.getKey(), (V) v);
		vSize++;
	}
	/**
	 * Agrega un nuevo edge entre dos vertices
	 * @param idVertexIni id del vertice inicial
	 * @param idVertexFin id del vertice final
	 * @param infoArc informacion del arco
	 */
	public void addEdge(K idVertexIni, K idVertexFin, A infoEdge) {
		Vertex v1=(Vertex) vertexTH.get(idVertexIni);
		Vertex v2=(Vertex) vertexTH.get(idVertexFin);
		if(v1!=null && v2!=null) {
			Edge e = new Edge(v1,v2,infoEdge);
			String key = v1.getKey() + "-" + v2.getKey();
			edgesTH.put(key, e);
			eSize++;
		}

	}



	/**
	 * Retorna la información de un vertice cuyo id ingresa por parámetro
	 * @param idVertex id del vertice
	 */
	public V getInfoVertex(K idVertex) {
		Vertex v = (Vertex) vertexTH.get(idVertex);
		return (V) v.getValue();
	}

	public void setInfoVertex(K idVertex, V infoVertex) {
		Vertex v = (Vertex) vertexTH.get(idVertex);
		v.setValue(infoVertex);
	}

	public Vertex getVertex(K idVertex) {
		return (Vertex) vertexTH.get(idVertex);
	}

	public Edge getEdge(String key){
		return edgesTH.get(key);
	}
	/**
	 * Retorna la información de un edge que conecta dos vertices con id ingresados por parámetro
	 * @param idVertexIni id del vertice inicial
	 * @param idVertexFin id del vertice final
	 * @return Informacion del edge asociado a los dos vertices, null en caso de no existir
	 */
	public A getInfoEdge(K idVertexIni, K idVertexFin) {
		int idIni = (int) idVertexIni;
		int idFin = (int) idVertexFin;
		String key = idIni+"-"+idFin;
		Edge e = edgesTH.get(key);
		return (A) e.getInfo();
	}



	/**
	 * Permite asignar la informacion a un edge con un vertice inicial y final recibidos por parametro
	 * @param idVertexIni id del vertice inicial	
	 * @param idVertexFin id del vertice final
	 * @param infoArc informacion a asignar
	 */
	public void setInfoEdge(K idVertexIni, K idVertexFin, A infoEdge) {
		int idIni = (int) idVertexIni;
		int idFin = (int) idVertexFin;
		String key = idIni+"-"+idFin;
		Edge e = edgesTH.get(key);
		e.setInfo(infoEdge);
		edgesTH.put(key, e);
	}

	/**
	 * Retorna un iterador con los id de los vertices adyacentes a uno ingresado por parametro
	 * @param idVertex id del vertice cuyos adyacentes se quieren retornar
	 */
	public ListIterator<K> adj (K idVertex){
		LinkedList<K> adjs = new LinkedList<K>();
		for(String s: edgesTH.keys())
		{
			String[] spl = s.split("-");
			if(Integer.parseInt(spl[0])==(int)idVertex){
				K ady = (K) spl[1];
				adjs.add(ady);
			}
		}
		return adjs.iterator();
	}
	
	/**
	 * Retorna las llaves de la tabla de edges
	 */
	public Iterable<String> edgeKeys (){
		return edgesTH.keys();
	}

	/**
	 * Retorna el grado de un vertice con id recibido por parametro
	 * @param idVertex id del vertice recibida por parametro
	 * @return grado del vertice en caso de existir, -1 de lo contrario
	 */
	public int degree(K idVertex) {
		int x = 0;
		ListIterator<K> iter = adj(idVertex);
		while(iter.hasNext()){
			iter.next();
			x++;
		}
		
		return x;
	}


}



