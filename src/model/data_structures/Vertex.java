package model.data_structures;

public class Vertex <Key extends Comparable<Key>, Value> {

	public enum Tipo{
		INTERSECCION,
		ESTACION;
	}
	
	private Tipo tipo;
	/**
	 * Llave del vertice
	 */
	private Key key;
	
	/**
	 * Valor del vertice
	 */
	private Value val;
	
	/**
	 * Boolean que permite saber si el vertice ya fue visitado en un recorrido
	 */
	private boolean marked;

	/**
	 * Crea un vertice con un valor y una llave ingresadas por parámetro
	 * @param k llave del vertice
	 * @param v valor del vertice
	 */
	public Vertex(Key k, Value v) {
		key=k;
		val=v;
		tipo=null;
		marked=false;
	}
	
	/**
	 * Agrega un edge a la lista de edges del vertice
	 * @param e edge a agregar a la lista
	 */

	public void setTipo(Tipo pTipo) {
		tipo = pTipo;
	}
	/**
	 * Retorna la llave del vertice
	 */
	public Key getKey() {
		return key;
	}
	
	/**
	 * Retorna el valor del vertice
	 */
	public Value getValue() {
		return val;
	}
	
	/**
	 * Le asocia un valor al vertice, el cual se ingresa por parámetro
	 * @param pVal valor a agregar
	 */
	public void setValue(Value pVal) {
		val=pVal;
	}
	
	public boolean isMarked() {
		return marked;
	}
	
	public Tipo getTipo() {
		return tipo;
	}

}
