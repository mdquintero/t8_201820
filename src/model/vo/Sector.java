package model.vo;

public class Sector {

	private double longI;

	private double longF;

	private double latI;

	private double latF;
	
	private Route[] rutas;
	
	private int id;
	
	public Sector(double loi, double lof, double lai, double laf, int id) {
		longI=loi;
		longF=lof;
		latI=lai;
		latF=laf;
		rutas=new Route[0];
		this.id = id;
	}

	//METODOS RETURN
	public int getID()
	{
		return id;
	}
	
	public double longInicial() {
		return longI;
	}

	public double longFinal() {
		return longF;
	}

	public double latInicial() {
		return latI;
	}

	public double latFinal() {
		return latF;
	}

	public void addRuta(Route ruta)
	{
		Route[] nueva = new Route[rutas.length+1];
		for(int i=0; i<rutas.length; i++)
		{
			nueva[i]=rutas[i];
		}
		nueva[nueva.length-1]=ruta;
		rutas = nueva;
		
	}
	
	public Route[] getRutas()
	{
		return rutas;
	}
	

}
