package model.logic;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;

import model.data_structures.Edge;
import model.data_structures.Graph;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.Vertex;

public class JSONWriter {
	
	private Graph graph;
	
	public JSONWriter(Graph pGraph){
		graph=pGraph;
	}

	public void writeJsonStream(String rutaFile) throws IOException {
		 FileOutputStream out = new FileOutputStream(rutaFile);
	     JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
	     writer.setLenient(true);
	     writer.setIndent("    ");
	     writeNodesArray(writer);
	     writeEdgesArray(writer);
	     writer.close();
	   }
	
	public void writeNodesArray(JsonWriter writer) throws IOException {
		 writer.beginObject();
		 writer.name("nodos");
	     writer.beginArray();
	     for (int i=0;i<Manager.MAX_SIZE;i++) {
	    	 if(graph.getVertex(i)!=null)
	    		 writeNode(writer, graph.getVertex(i));
	     }
	     writer.endArray();
	     writer.endObject();
	   }
	
	public void writeNode(JsonWriter writer, Vertex v) throws IOException{
		writer.beginObject();
		String se = (String) v.getValue();
		String[] s=se.split(":");
		writer.name("idNodo").value((int) v.getKey());
		writer.name("latitud").value(Double.parseDouble(s[1]));
		writer.name("longitud").value(Double.parseDouble(s[0]));
		writer.name("tipo").value(v.getTipo().toString());
		writer.endObject();
	}
	
	public void writeEdgesArray(JsonWriter writer) throws IOException{
		writer.beginObject();
		writer.name("edges");
		writer.beginArray();
		Iterable<String> iter = graph.edgeKeys();
		for(String s: iter) {
			Edge e = graph.getEdge(s);
			writeEdge(writer, e);
		}
		writer.endArray();
		writer.endObject();
	}
	
	public void writeEdge(JsonWriter writer, Edge edge) throws IOException{
		writer.beginObject();
		writer.name("idSalida").value((int)edge.getStartVertex().getKey());
		writer.name("idLlegada").value((int)edge.getEndVertex().getKey());
		writer.name("distancia").value((double) edge.getInfo());
		writer.endObject();
	}

}
