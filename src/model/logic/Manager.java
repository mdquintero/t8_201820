package model.logic;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.lang.Math;

import com.google.gson.stream.JsonWriter;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import API.IManager;
import model.data_structures.Edge;
import model.data_structures.Graph;
import model.data_structures.LinearProbingTH;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.PrintGraph;
import model.data_structures.Vertex;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class Manager<K extends Comparable<K>, V, A> implements IManager {

	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos de nodos
	public static final String VERTEX = rutaGeneral+"Nodes_of_Chicago_Street_Lines.txt";

	//Ruta del archivo de lista de adyacencia
	public static final String ADJACENCY_LIST = rutaGeneral+"Adjacency_list_of_Chicago_Street_Lines.txt";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = rutaGeneral+"Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv";
	
	public final static String GRAFO_JSON = rutaGeneral+"ChicagoGraph.json";
	
	public static final int MAX_ID = 321374; 
	
	public final static int MAX_SIZE=MAX_ID+626;

	private Graph<Integer,String,Double> graph = new Graph<Integer,String, Double>();

	private PrintGraph printer;
	
	private static LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();

	private Trip[] arregloTrips;

	//M�TODOS DE CARGA

	/**
	 * Carga las estaciones por par�metro
	 */
	public void cargarStations(String rutaStations) {
		//cargar stations
		try{
			FileReader fr = new FileReader(new File(rutaStations));
			CSVReader br = new CSVReader(fr);

			String[] line = br.readNext();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readNext();

			while(line!=null)
			{
				String[] fecha = (line[6].replace(' ', '/').replace(':', '/')).split("/");
				int tamano = fecha.length;
				Integer[]fecha2 = new Integer[tamano];
				for(int i = 0; i< fecha2.length; i++)
				{
					fecha2[i] = Integer.parseInt(fecha[i]);
				}
				LocalDateTime ini = LocalDateTime.of(fecha2[2], fecha2[0], fecha2[1],fecha2[3],fecha2[4],tamano==6 ? fecha2[5]:00);
				Station s = new Station(Integer.parseInt(line[0]), line[1],ini, Double.parseDouble(line[3]),Double.parseDouble(line[4]));


				listaEncadenadaStations.add(s);

				line = br.readNext();
			}

			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Carga los vertices INTERSECCION
	 * @param rutaVertices la ruta 
	 */
	public void cargarVertices(String rutaVertices) {
		try {
			FileReader fr = new FileReader(new File(rutaVertices));
			CSVReader br = new CSVReader(fr);

			String[] line = br.readNext();

			while(line!=null) {
				int key = Integer.parseInt(line[0]);
				String value = line[1]+":"+line[2];
				graph.addVertex(key, value,0);
				line=br.readNext();
			}
			br.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	public void cargarAdjacency(String rutaAdjacency) {
		try {
			FileReader fr = new FileReader(new File(rutaAdjacency));
			CSVParser parser =new CSVParserBuilder().withSeparator(' ').build();
			CSVReader br = new CSVReaderBuilder(fr).withCSVParser(parser).build();
			
			int c=0;

			String[] line = br.readNext();
			//Salto de lineas sin informacion
			line=br.readNext();
			line=br.readNext();
			line=br.readNext();


			while(line!=null) {
				int size = line.length;
				int idVertexIni = Integer.parseInt(line[0]);
				String[] startCoords=graph.getInfoVertex(idVertexIni).split(":");
				if(size>1) {
					for(int i=1; i<size;i++) {
						int idVertexFin = Integer.parseInt(line[i]);
						String[] endCoords=graph.getInfoVertex(idVertexFin).split(":");
						double dist = calculateDistance(Double.parseDouble(endCoords[1]), Double.parseDouble(endCoords[0]),
								Double.parseDouble(startCoords[0]), Double.parseDouble(startCoords[1]));
						graph.addEdge(idVertexIni, idVertexFin, dist);
					}
				}
				line=br.readNext();
				c++;
			}
			br.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void writeJson(String rutaFile) throws IOException {
		JSONWriter writer = new JSONWriter(graph);
		writer.writeJsonStream(rutaFile);
	}
	
	public void readJson(String rutaFile) throws IOException {
		JSONReader<Integer , String, Double> reader = new JSONReader<Integer,String,Double>();
		graph=reader.readJsonStream(rutaFile);
	}

	public void print()
	{
		printer=new PrintGraph(graph);
		printer.imprimir();
	}
	
	//METODOS AUXILIARES
	public void agregarStations()
	{
		ListIterator<Station> ite = listaEncadenadaStations.iterator();
		while(ite.hasNext())
		{
			Station actual = ite.next();
			double lati = actual.getLat();
			String sLati = ""+lati;
			double longi = actual.getLongitude();
			String sLongi = ""+longi;
			String value = sLongi+":"+sLati;
			int ID = encontrarVertexCercano(longi, lati);
			String info = (String) graph.getInfoVertex(ID);
			String[] epa = info.split(":");
			double lat1 = Double.parseDouble(epa[1]);
			double lon1 = Double.parseDouble(epa[0]);
			double distance= calculateDistance(lat1, lon1, longi, lati);
			int idStation = MAX_ID+actual.getStationId();
			graph.addVertex(idStation, value, 1);
			graph.addEdge(idStation, ID, distance);
		}
	}
	
	public int encontrarVertexCercano(double longi, double lati)
	{
		int id = 0;
		double disMenor = 0;
		for(int i=0; i<=MAX_ID;i++)
		{
			String info = graph.getInfoVertex(i);
			String[] epa = info.split(":");
			double lat1 = Double.parseDouble(epa[1]);
			double lon1 = Double.parseDouble(epa[0]);
			double disActual= calculateDistance(lat1, lon1, longi, lati);
			if(disMenor == 0)
			{
				disMenor = disActual;
				id = i;
			}
			
			if(disMenor>disActual)
			{
				disMenor = disActual;
				id = i;
			}
			
		}
		
		return id;
	}
	/**
	 * Calcula la distancia entre dos estaciones
	 * @param inicio Id estacion inicial
	 * @param end Id estacion final
	 * @return Distancia entre dos estaciones
	 */
	public static double calcularDistancia(int inicio, int end)
	{
		boolean ini = false;
		boolean fini = false;
		Station referencia = null;
		Station ended = null;
		boolean yatodo=false;

		double x = 0;

		ListIterator<Station> listite = (ListIterator<Station>) listaEncadenadaStations.iterator();

		while(listite.hasNext()&&!yatodo) {

			Station s = listite.next();
			if(s.getStationId()==inicio)
			{
				referencia = s;
				ini = true;
			}
			if(s.getStationId()==end)
			{
				ended = s;
				fini = true;
			}
			if(ini&&fini) {
				yatodo=true;
			}
		}
		if(referencia != null && ended != null)
		{
			x = calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}

		return x;
	}


	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public static double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}



}
