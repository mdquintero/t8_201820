package model.logic;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.google.gson.stream.JsonReader;

import model.data_structures.Edge;
import model.data_structures.Graph;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.Vertex;
import model.data_structures.Vertex.Tipo;

public class JSONReader <K extends Comparable<K>,V, A> {

	public JSONReader(){

	}


	public Graph<Integer, String, Double> readJsonStream(String rutaFile) throws IOException{
		FileInputStream in = new FileInputStream(rutaFile);
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		reader.setLenient(true);
		Graph<Integer, String, Double> graphi = new Graph<Integer, String, Double>();
		try {
			LinkedList<Vertex> vList = readNodesArray(reader);
			LinkedList<Edge> eList = readEdgesArray(reader);
			ListIterator<Vertex> vIter = vList.iterator();
			ListIterator<Edge> eIter = eList.iterator();
			
			//agrega todos los vertices al grafo
			while(vIter.hasNext()) {
				Vertex v = vIter.next();
				graphi.addVertex(v);
			}

			//agrega todos los arcos al grafo, considerando que no se repita ninguno
			while(eIter.hasNext()) {
				Edge e = eIter.next();
				e.actuallyBecomeEdge(graphi);
				if(e.getStartVertex()!=null && e.getEndVertex()!=null)
					graphi.addEdge( (int) e.getStartVertex().getKey(), (int) e.getEndVertex().getKey(), (double) e.getInfo());
			}

		}
		finally {
			System.out.println(graphi.vSize());
			System.out.println(graphi.eSize());
			reader.close();
		}
		return graphi;
	}

	public LinkedList<Vertex> readNodesArray(JsonReader reader) throws IOException{
		LinkedList<Vertex> list = new LinkedList<Vertex>();

		reader.beginObject();
		reader.nextName();
		reader.beginArray();
		
		while(reader.hasNext()) {
			list.add(readNode(reader));
		}
		
		reader.endArray();
		reader.endObject();

		return list;
	}

	
	public Vertex readNode(JsonReader reader) throws IOException {
		int id=0;
		String lat="";
		String longi="";
		Tipo tipo=null;
		
		reader.beginObject();
		while(reader.hasNext()) {
			String name = reader.nextName();
			if(name.equals("idNodo")) {
				id = (int) reader.nextLong();
			}
			else if(name.equals("latitud")) {
				lat=""+reader.nextDouble();
			}
			else if(name.equals("longitud")) {
				longi=""+reader.nextDouble();
			}
			else{
				String sTipo = reader.nextString();
				if(sTipo.equals("INTERSECCION"))
					tipo=Tipo.INTERSECCION;
				else
					tipo=Tipo.ESTACION;
			}

		}
		reader.endObject();
		String value = lat+":"+longi;
		Vertex<Integer, String> v = new Vertex<Integer, String>(id,value);
		v.setTipo(tipo);
		return v;
	}
	
	

	public LinkedList<Edge> readEdgesArray(JsonReader reader) throws IOException{
		LinkedList<Edge> list = new LinkedList<Edge>();

		reader.beginObject();
		reader.nextName();
		reader.beginArray();
		
		while(reader.hasNext()) {
			list.add(readEdge(reader));
		}
		
		reader.endArray();
		reader.endObject();

		return list;
	}

	public Edge readEdge(JsonReader reader) throws IOException {
		int startId=0;
		int endId=0;
		double info=0;

		reader.beginObject();
		while(reader.hasNext()) {
			String name = reader.nextName();
			if(name.equals("idSalida"))
				startId=(int) reader.nextLong();
			else if(name.equals("idLlegada"))
				endId=(int) reader.nextLong();
			else
				info=reader.nextDouble();
		}
		reader.endObject();
		return new Edge(startId, endId, info);
	}
}
