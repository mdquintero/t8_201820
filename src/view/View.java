package view;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class View {

	public static void main(String[] args){

		Controller control = new Controller();
		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{

			case 1:  //Carga de nodes

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				Controller.cargarVertices(Manager.VERTEX);

				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;

			case 2: //carga de adjacency list
				
				//Memoria y tiempo
				long memoryBeforeCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime2 = System.currentTimeMillis();

				Controller.cargarAdjacency(Manager.ADJACENCY_LIST);

				//Tiempo en cargar
				long endTime2 = System.currentTimeMillis();
				long duration2 = endTime2 - startTime2;

				//Memoria usada
				long memoryAfterCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration2 + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase2 - memoryBeforeCase2)/1000000.0) + " MB");
				break;
				
			case 3:
				long startTime3 = System.currentTimeMillis();
				control.cargarEstaciones(Manager.STATIONS_Q3_Q4);
				control.agregarStations();
				long endTime3 = System.currentTimeMillis();
				long duration3 = endTime3 - startTime3;
				System.out.println("Se demor�: " + duration3 + " milisegundos");
				break;
				
			case 4:
				long startTime4 = System.currentTimeMillis();

				try {
					Controller.writeJson(Manager.GRAFO_JSON);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				long endTime4 = System.currentTimeMillis();
				long duration4 = endTime4 - startTime4;
				System.out.println("Se demor�: " + duration4 + " milisegundos");
				break;
				
			case 5:
				long startTime5 = System.currentTimeMillis();

				try {
					Controller.readJson(Manager.GRAFO_JSON);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				long endTime5 = System.currentTimeMillis();
				long duration5 = endTime5 - startTime5;
				System.out.println("Se demor�: " + duration5 + " milisegundos");
				break; 
				
			case 6:
				Controller.print();
				break;
				
			case 7: //Salir
					fin = true;
					linea.close();
					break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Taller 8 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con la fuente de datos Nodes_of_Chicago_Street_Lines");
		System.out.println("2. Actualizar la informacion del sistema con la fuente de datos Adjacency_list_of_Chicago_Street_Lines");
		System.out.println("3. Actualizar la informacion del sistema con la fuente de datos de Estaciones Q3 y Q4");
		System.out.println("4. Escribir un JSON con la informacion del grafo");
		System.out.println("5. Leer un JSON con la informacion para cargar el grafo");
		System.out.println("6. Crear un html del mapa con grafos");
		System.out.println("7. Salir");


	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

}
