package controller;

import java.io.IOException;
import java.time.LocalDateTime;

import API.IManager;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static Manager manager = new Manager();
    
    public static void cargarEstaciones(String rutaEstaciones){
    	manager.cargarStations(rutaEstaciones);
    }

    public static void cargarVertices(String rutaVertices) {
    	manager.cargarVertices(rutaVertices);
    }
    
    public static void cargarAdjacency(String rutaAdjacency) {
    	manager.cargarAdjacency(rutaAdjacency);
    }
 
    public static void agregarStations()
    {
    	manager.agregarStations();
    }
    
    public static void writeJson(String rutaFile) throws IOException {
    	manager.writeJson(rutaFile);
    }
    
    public static void readJson(String rutaFile) throws IOException{
    	manager.readJson(rutaFile);
    }
    
    public static void print()
    {
    	manager.print();
    }
}
